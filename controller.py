#!/usr/bin/env python

from flask import Flask, jsonify, redirect, render_template, request, Response
# from werkzeug.exceptions import BadRequest, NotFound

import models

# Initialize Flask application
app = Flask(__name__, template_folder='views')


@app.route('/')
def index():
    """Renders main page."""
    return render_template('home.html')


@app.route('/home')
def home():
    """Redirects to '/'."""
    return redirect('/', code=302)


@app.route('/bottle')
def bottle():
    """Renders results page with bottle data."""
    # Get params from request
    params = request.args.to_dict()
    # Instantiate query class
    bottle_data = models.QueryBeerDb()
    # Call get_dbdata() method on bottle table with params
    results = bottle_data.get_dbdata('bottle', params)
    # Return results to results page
    return Response(results, mimetype='application/json')


@app.route('/bottle_breweries')
def bottle_breweries():
    """Renders results page with bottle_breweries join table data."""
    params = request.args.to_dict()
    bottle_breweries_data = models.QueryBeerDb()
    results = bottle_breweries_data.get_dbdata('bottle_breweries', params)
    return Response(results, mimetype='application/json')


@app.route('/brewery')
def brewery():
    """Renders results page with brewery data."""
    params = request.args.to_dict()
    brewery_data = models.QueryBeerDb()
    results = brewery_data.get_dbdata('brewery', params)
    return Response(results, mimetype='application/json')


@app.route('/photo')
def photo():
    """Renders results page with photo data."""
    params = request.args.to_dict()
    photo_data = models.QueryBeerDb()
    results = photo_data.get_dbdata('photo', params)
    return Response(results, mimetype='application/json')


@app.route('/style')
def style():
    """Renders results page with style data."""
    params = request.args.to_dict()
    style_data = models.QueryBeerDb()
    results = style_data.get_dbdata('style', params)
    return Response(results, mimetype='application/json')


@app.route('/bottle/available_fields')
def bottle_fields():
    """Renders results page with bottle available fields."""
    # Instantiate query class
    bottle_data = models.QueryBeerDb()
    # Call get_available_fields() method on bottle table
    results = bottle_data.get_available_fields('bottle')
    # Return results to results page
    return Response(results, mimetype='application/json')


@app.route('/bottle_breweries/available_fields')
def bottle_breweries_fields():
    """Renders results page with bottle_breweries available fields."""
    bottle_breweries_data = models.QueryBeerDb()
    results = bottle_breweries_data.get_available_fields('bottle_breweries')
    return Response(results, mimetype='application/json')


@app.route('/brewery/available_fields')
def brewery_fields():
    """Renders results page with brewery available fields."""
    brewery_data = models.QueryBeerDb()
    results = brewery_data.get_available_fields('brewery')
    return Response(results, mimetype='application/json')


@app.route('/photo/available_fields')
def photo_fields():
    """Renders results page with photo available fields."""
    photo_data = models.QueryBeerDb()
    results = photo_data.get_available_fields('photo')
    return Response(results, mimetype='application/json')


@app.route('/style/available_fields')
def style_fields():
    """Renders results page with style available fields."""
    style_data = models.QueryBeerDb()
    results = style_data.get_available_fields('style')
    return Response(results, mimetype='application/json')


if __name__ == '__main__':
    app.run(debug=True)
