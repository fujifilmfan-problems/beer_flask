#!/usr/bin/env python

config = {
    'bottle': {
        'excluded_fields': ['series', 'created_at', 'updated_at'],
        'fields_stem': 'SELECT column_name FROM information_schema.columns '
                       'WHERE table_name = \'bottle\';',
        'query_limit': 'LIMIT 500',
        'query_stem': 'SELECT bottle_id, beer_name, style_id, '
                      'CAST(volume_number AS CHAR) AS volume_number, '
                      'volume_unit, alcohol, year, link FROM bottle '
    },
    'bottle_breweries': {
        'excluded_fields': ['created_at', 'updated_at'],
        'fields_stem': 'SELECT column_name FROM information_schema.columns '
                       'WHERE table_name = \'bottle_breweries\';',
        'query_limit': 'LIMIT 500',
        'query_stem': 'SELECT id, bottle_id, brewery_id FROM bottle_breweries '
    },
    'brewery': {
        'excluded_fields': ['created_at', 'updated_at'],
        'fields_stem': 'SELECT column_name FROM information_schema.columns '
                       'WHERE table_name = \'brewery\';',
        'query_limit': 'LIMIT 500',
        'query_stem': 'SELECT brewery_id, full_name, short_name, locality, '
                      'region, country, continent FROM brewery '
    },
    'photo': {
        'excluded_fields': ['created_at', 'updated_at'],
        'fields_stem': 'SELECT column_name FROM information_schema.columns '
                       'WHERE table_name = \'photo\';',
        'query_limit': 'LIMIT 500',
        'query_stem': 'SELECT photo_id, priority, angle, bottle_id FROM photo '
    },
    'style': {
        'excluded_fields': ['created_at', 'updated_at'],
        'fields_stem': 'SELECT column_name FROM information_schema.columns '
                       'WHERE table_name = \'style\';',
        'query_limit': 'LIMIT 500',
        'query_stem': 'SELECT style_id, style_specific, style_general, '
                      'style_alternative, type FROM style '
    }
}
