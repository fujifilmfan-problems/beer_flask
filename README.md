beer_flask
----------

### Overview
---
The API has endpoints for each of the five tables in beerdb and allows arguments that map to conditions in a WHERE 
clause (linked by AND).  There is also a `/[table]/available_fields` endpoint for each table that lists selected 
columns available to use as arguments.

The five tables are:
* bottle
* brewery
* bottle_breweries
* photo
* style

### Project Set Up
---
This project uses `Poetry` and `venv`.  
`$ poetry init`  
`$ poetry install --no-root`  
`$ source .venv/bin/activate` (so I don't need `poetry run`)  

### Example use
---
1. Make sure the database is running (`mysql.server start` or `brew services start mariadb`)
2. Run `controller.py`.
3. Navigate to 127.0.0.1:5000.

Names of breweries, bottles, countries, etc. must be in quotes (single or double).

The home page (`/` or `/home`) has a button for displaying all breweries. 
![Screenshot of '/'](images/Screen Shot 2019-03-10 at 20.00.02.png)  

This is the result of clicking the button on the home page.
![Screenshot of '/brewery?'](images/Screen Shot 2019-03-10 at 20.00.21.png)  

Sample `/[table]/available_fields` call.
![Screenshot of '/brewery/available_fields'](images/Screen Shot 2019-03-10 at 20.00.49.png)  

Requesting breweries from a particular region.
![Screenshot of '/brewery?region='CA'](images/Screen Shot 2019-03-10 at 20.01.31.png)

Requesting breweries from a particular locality.
![Screenshot of '/brewery?locality='San Marcos'](images/Screen Shot 2019-03-10 at 20.02.35.png)  

Sample data from bottle_breweries join table.
![Screenshot of '/bottle_breweries?brewery_id=62](images/Screen Shot 2019-03-10 at 20.03.24.png)  

All bottles with the name "Stone IPA".
![Screenshot of '/bottle?beer_name='Stone IPA'](images/Screen Shot 2019-03-10 at 20.03.56.png)  

All possible beer styles in the database.
![Screenshot of '/style'](images/Screen Shot 2019-03-10 at 20.20.27.png)  

