#!/usr/bin/env python

import config
import json
import mysql.connector

HOSTNAME = 'localhost'
USERNAME = ''
PASSWORD = ''
DATABASE = 'beerdb'


class QueryBeerDb(object):

    def get_dbdata(self, table, params):
        # Construct WHERE clause from params
        where = self.construct_where_clause(params) if params else None
        # Construct query from table name and WHERE clause
        query = self.construct_query(table, where)
        # Execute the query
        results = self.execute_query(query)
        # Convert list of tuples to JSON
        # `ensure_ascii=False` ensures unusual characters are passed to
        # the controller correctly
        formatted_results = json.dumps(results, ensure_ascii=False)
        return formatted_results

    def get_available_fields(self, table):
        # Get available fields query stem from config.py
        query = config.config[table]['fields_stem']
        # Execute query
        all_fields = self.execute_query(query)

        filtered_fields = []
        # Add tuples from the query to filtered_fields if the field is
        # not in excluded_fields
        for field in all_fields:
            for k, v in field.items():
                if v not in config.config[table]['excluded_fields']:
                    filtered_fields.append({k: v})
        # Convert list of tuples to JSON
        print(filtered_fields)
        formatted_results = json.dumps(filtered_fields)
        return formatted_results

    @staticmethod
    def construct_where_clause(params):
        conditions = []
        # Convert dict items like 'short_name': 'Stone' to strings like
        # 'short_name=Stone' and add to conditions list
        for k, v in params.items():
            conditions.append(f'{k}={v}')
        # Join string segments with AND; there is no support for OR yet
        where_clause = 'WHERE ' + ' AND '.join(conditions) + ' '
        return where_clause

    @staticmethod
    def construct_query(table, where):
        # Lookup stem (SELECT, FROM) in config
        stem = config.config[table]['query_stem']
        # Lookup LIMIT in config
        limit = config.config[table]['query_limit']
        # Create query from stem, WHERE, and LIMIT
        if where:
            query = f'{stem}{where}{limit};'
        else:
            query = f'{stem}{limit};'
        return query

    def execute_query(self, query):
        connection = mysql.connector.connect(
            host=HOSTNAME, user=USERNAME, passwd=PASSWORD, db=DATABASE)
        cursor = connection.cursor()
        cursor.execute(query)
        raw_results = cursor.fetchall()
        results = self.convert_to_dict(cursor, raw_results)
        cursor.close()
        connection.close()
        return results

    @staticmethod
    def convert_to_dict(cursor, raw_results):
        """Returns a list of dictionaries"""
        # cursor.description contains data like [('brewery_id', 3,
        # None, None, None, None, 0, 16899), ('full_name', 253,
        # None, None, None, None, 0, 4097),...] where key[0] of each
        # tuple is the column name
        keys = []
        for key in cursor.description:
            keys.append(key[0])
        # 'results' will be a list of dictionaries
        results = [ dict(zip(keys, row)) for row in raw_results]
        return results
