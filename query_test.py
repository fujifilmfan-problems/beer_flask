#!/usr/bin/env python

import mysql.connector

HOSTNAME = 'localhost'
USERNAME = ''
PASSWORD = ''
DATABASE = 'beerdb'


def execute_query(connection):
    cursor = connection.cursor()
    query = "select * from brewery"
    cursor.execute(query)
    result = cursor.fetchall()
    for row in result:
        print(row)


if __name__ == '__main__':
    connection = mysql.connector.connect(
        host=HOSTNAME, user=USERNAME, passwd=PASSWORD, db=DATABASE)
    execute_query(connection)
    connection.close()
